package com.epam_shapes;

public class Triangle extends Shape{
    Triangle(String color){
        setColor(color);
    }
    public double countSquare(double firstSide,double secondSide,double thirdSide){
        double p =(firstSide+secondSide+thirdSide)/2;
        this.square =Math.sqrt(p*(p-firstSide)*(p-secondSide)*(p-thirdSide));
        return square;
    }
}
