package com.epam_shapes;

public class Main {
    public static void main(String[] args) {
        Triangle triangle =new Triangle("Yellow");
        Circle circle = new Circle("Red");
        Rectangle rectangle = new Rectangle("Brown");
        System.out.println("We have "+triangle.color+" triangle which square is "+triangle.countSquare(6,7,8) );
        System.out.println("We have "+rectangle.color+" rectangle which square is "+rectangle.countSquare(6,7) );
        System.out.println("We have "+circle.color+" circle which square is "+circle.countSquare(6) );
    }
}
