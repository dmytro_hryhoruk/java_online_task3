package com.epam;

import java.util.ArrayList;


public class Manager {
    String name;

    Manager(String name) {
        this.name = name;
        System.out.println("Hello! Welcome to my Zoo. My name is " + name + ". I am gonna show you everything here.");
    }

    public void theListOfAnimals(ArrayList<Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal.name);
        }
    }

    public void theFoodOfAnimals(ArrayList<Animal> animals) {

        for (Animal animal : animals) {
            System.out.print(animal.name + ": ");
            animal.eat();
        }
    }

    public void theSleepingPlace(ArrayList<Animal> animals) {
        for (Animal animal : animals) {
            System.out.print(animal.name + ": ");
            animal.sleep();
        }
    }

    public void theListWithWings(ArrayList<Bird> birds) {
        for (Bird bird : birds) {
            System.out.println(bird.name + ": ");
            bird.fly();
        }
    }

}