package com.epam;

public class Parrot extends Bird {
    Parrot(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println("I am vegan ,so I can eat only plants ");
    }

}
