package com.epam;


public class Fish extends Animal {
    Fish(String name) {
        super(name);
    }

    public void sleep() {
        System.out.println("I sleep in the water)");
    }

    public void eat() {
        System.out.println("I can eat anything(including your fingers,so be careful)");
    }

    public void swim() {
        System.out.println("I can swim(probably because I live in water)");
    }
}
