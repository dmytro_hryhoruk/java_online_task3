package com.epam;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Manager one = new Manager("Dmytro");
        Fish shark = new Fish("White Shark");
        Eagle eagle = new Eagle("Eagle");
        Parrot parrot = new Parrot("Kesha");
        Bird crow = new Bird("Crow");
        ArrayList<Animal> List1 = new ArrayList<Animal>();
        ArrayList<Bird> List2 = new ArrayList<Bird>();
        List1.add(parrot);
        List1.add(crow);
        List1.add(shark);
        List1.add(eagle);
        List2.add(eagle);
        List2.add(parrot);
        List2.add(crow);
        System.out.println("There are few options here:");
        System.out.println("1. Show the list of all animals in the Zoo");
        System.out.println("2. Get the information about food that each animal eats");
        System.out.println("3. About sleeping habits");
        System.out.println("4. Show only those who can fly");
        System.out.println("5. Exit");
        Scanner sc = new Scanner(System.in);
        Boolean number = true;
        while (number) {
            int choice = sc.nextInt();
            if (choice == 1) {
                one.theListOfAnimals(List1);
            } else if (choice == 2) {
                one.theFoodOfAnimals(List1);
            } else if (choice == 3) {
                one.theSleepingPlace(List1);
            } else if (choice == 4) {
                one.theListWithWings(List2);
            } else if (choice == 5)
                number = false;
        }

    }
}
