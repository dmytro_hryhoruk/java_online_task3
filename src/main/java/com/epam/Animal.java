package com.epam;

public abstract class Animal {
    String name;

    Animal(String name) {
        this.name = name;
    }

    abstract void sleep();

    abstract void eat();

}
