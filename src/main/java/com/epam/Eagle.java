package com.epam;

public class Eagle extends Bird {
    Eagle(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println("I eat only meat");
    }
}
