package com.epam;

public class Bird extends Animal {
    Bird(String name) {
        super(name);
    }

    public void sleep() {
        System.out.println("I sleep on the threes");
    }

    public void eat() {
        System.out.println("I eat anything (but I don't know that, so I can die even being surrounded by free foods )");
    }

    public void fly() {
        System.out.println("I have wings, so I've decided to learn how to use them");
    }
}
